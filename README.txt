Nimbus-JWT

README

Nimbus JWT is a JSON Web Token (JWT) implementation for Java with support for 
signatures (JWS), encryption (JWE) and web keys (JWK). 

JSON Web Token (JWT) is a means of representing claims to be transferred between
two parties. The claims in a JWT are encoded as a JSON object that may be 
digitally signed using JSON Web Signature (JWS) and/or encrypted using JSON Web 
Encryption (JWE).

The suggested pronunciation of JWT is the same as the English word "jot".

This package implements the following IETF drafts:

	* [JWT] draft-jones-json-web-token-10
	* [JWA] draft-ietf-jose-json-web-algorithms-02
	* [JWS] draft-ietf-jose-json-web-signature-02
	* [JWE] draft-ietf-jose-json-web-encryption-02
	* [JWK] draft-ietf-jose-json-web-key-02

This package started as a fork of the JWT class from the OpenInfoCard project.

Dependencies:

	* The BouncyCastle.org cryptography provider for Java.
	* Apache Commons Codec for Base64 and Base64URL encoding and decoding.
	* JSON Smart for highly efficient parsing and serialisation of JSON. 

[eof]
